/*
 * Copyright 2002-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.com.luiz.carlos.test.klb.domain.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.luiz.carlos.test.klb.domain.Client;
import br.com.luiz.carlos.test.klb.domain.TypeClient;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ClientRepositoryTests {

	@Autowired
	private ClientRepository clientRepository;

	@Test
	public void shouldSaveClientSucess() {

		Client client = new Client("LUIZ", new BigDecimal(87L), TypeClient.A);
		clientRepository.save(client);

		assertNotNull(client.getId());

	}

	@Test
	public void shouldRemoveClientSucessTest() {

		Client client = new Client("LUIZ", new BigDecimal(87L), TypeClient.A);
		clientRepository.save(client);

		clientRepository.delete(client);

		Optional<Client> search = clientRepository.findById(client.getId());

		assertFalse(search.isPresent());

	}
	
	

	@Test
	public void shouldFindByIdClientSucessTest() {

		Client client = new Client("LUIZ", new BigDecimal(87L), TypeClient.A);
		clientRepository.save(client);

		Optional<Client> search = clientRepository.findById(client.getId());

		assertTrue(search.isPresent());
		
		assertEquals(client,search.get());

	}
	
	
}
