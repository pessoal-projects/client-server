package br.com.luiz.carlos.test.klb.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.luiz.carlos.test.klb.dto.ClientDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ClientControllerIntegrationTest {

	@LocalServerPort
	private int port;

	private URL base;

	@Autowired
	private TestRestTemplate template;

	@Before
	public void setUp() throws Exception {
		this.base = new URL("http://localhost:" + port + "/");
	}

	@Test
	public void shouldAddClient() throws Exception {
		ClientDTO clientDTO = new ClientDTO.Builder().setName("LUIZ CARLOS").setTypeClient("A").setCreditLimit(new BigDecimal(87)).build();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<ClientDTO> entity = new HttpEntity<>(clientDTO, headers);

		ResponseEntity<ClientDTO> response = template.postForEntity(base.toString().concat("add"), entity,
				ClientDTO.class);
		assertNotNull(response);
		assertNotNull(response.getBody());
		assertNotNull(response.getStatusCode());
		assertEquals(response.getStatusCode(), HttpStatus.OK);

	}
	
	
	
	
}
