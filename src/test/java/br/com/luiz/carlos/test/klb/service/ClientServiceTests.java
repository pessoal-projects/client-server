package br.com.luiz.carlos.test.klb.service;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.luiz.carlos.test.klb.domain.Client;
import br.com.luiz.carlos.test.klb.domain.TypeClient;
import br.com.luiz.carlos.test.klb.domain.repository.ClientRepository;
import br.com.luiz.carlos.test.klb.service.impl.ClientServiceImpl;

@RunWith(SpringRunner.class)
@ContextConfiguration
public class ClientServiceTests {

	@Configuration
	static class Config {

		@Bean
		public ClientService clientService() {
			ClientService clientService = new ClientServiceImpl();
			return clientService;
		}
	}

	@MockBean
	private ClientRepository clientRepository;

	@Autowired
	private ClientService clientService;

	@Test
	public void shouldSaveClientSucess() {
		Client client = new Client("Luiz",new BigDecimal(10l), TypeClient.A);
		when(clientRepository.save(any())).then(new Answer<Client>() {

			@Override
			public Client answer(InvocationOnMock ioMock) throws Throwable {
				Client client = (Client) ioMock.getArguments()[0];
				client.setId(new Random().nextLong());
				return client;
			}

		});

		clientService.save(client);
		assertNotNull(client.getId());

	}

	
}
