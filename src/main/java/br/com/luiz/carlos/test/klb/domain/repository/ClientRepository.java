package br.com.luiz.carlos.test.klb.domain.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.luiz.carlos.test.klb.domain.Client;

public interface ClientRepository extends CrudRepository<Client, Long> {

	@Override
	List<Client> findAll();

}
