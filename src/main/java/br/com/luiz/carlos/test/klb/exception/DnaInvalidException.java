package br.com.luiz.carlos.test.klb.exception;

public class DnaInvalidException extends Exception {

	public DnaInvalidException(String message) {
		super(message);
	}

}
