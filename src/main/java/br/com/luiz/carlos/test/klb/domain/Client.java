package br.com.luiz.carlos.test.klb.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Client implements Serializable{
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String name;
	private BigDecimal creditLimit;
	private TypeClient typeClient;
	private BigDecimal interestCharge;
	
	public BigDecimal getInterestCharge() {
		return interestCharge;
	}

	public void setInterestCharge(BigDecimal interestCharge) {
		this.interestCharge = interestCharge;
	}

	public Client() {
		super();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}
	public TypeClient getTypeClient() {
		return typeClient;
	}
	public void setTypeClient(TypeClient typeClient) {
		this.typeClient = typeClient;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Client(String name, BigDecimal creditLimit, TypeClient typeClient) {
		super();
		this.name = name;
		this.creditLimit = creditLimit;
		this.typeClient = typeClient;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
	public static class Builder {

        private Long id;
        private String name;
        private TypeClient typeClient;
        private BigDecimal creditLimit;
        private BigDecimal interestCharge;

        public Builder setId(Long id) {
            this.id = id;
            return this; 
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }
        
        public Builder setTypeClient(String type) {
            this.typeClient = TypeClient.valueOf(type);
            return this;
        }
        
        public Builder setCreditLimit(BigDecimal creditLimit) {
            this.creditLimit = creditLimit;
            return this;
        }

        public Client build(){
        	Client client= new Client();
        	client.setName(this.name);
        	client.setId(this.id);
        	client.setTypeClient(this.typeClient);
        	client.setCreditLimit(creditLimit);
        	client.setInterestCharge(this.interestCharge);
        	return client;
        }
    }
	
	
	
	
	

}
