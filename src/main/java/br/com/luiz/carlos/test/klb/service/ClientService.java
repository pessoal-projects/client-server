package br.com.luiz.carlos.test.klb.service;

import java.util.List;

import br.com.luiz.carlos.test.klb.domain.Client;

public interface ClientService {

	public Client save(Client client);

	public List<Client> findAll();

	

}
