package br.com.luiz.carlos.test.klb.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.luiz.carlos.test.klb.domain.Client;
import br.com.luiz.carlos.test.klb.domain.repository.ClientRepository;
import br.com.luiz.carlos.test.klb.service.ClientService;

@Service
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientRepository clientRepository;

	public Client save(Client client) {

		client.setInterestCharge(calculateInterestCharge(client));
		return clientRepository.save(client);
	}

	private BigDecimal calculateInterestCharge(Client client) {

		switch (client.getTypeClient()) {
		case A:

			return new BigDecimal(0);

		case B:

			return new BigDecimal(10);

		case C:

			return new BigDecimal(20);

		}

		return null;
	}

	@Override
	public List<Client> findAll() {
		return this.clientRepository.findAll();
	}

}
