package br.com.luiz.carlos.test.klb.controller;

import static org.springframework.http.HttpStatus.OK;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.luiz.carlos.test.klb.domain.Client;
import br.com.luiz.carlos.test.klb.dto.ClientDTO;
import br.com.luiz.carlos.test.klb.service.ClientService;

@RestController
public class ClientController {

	@Autowired
	private ClientService clientService;

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> add(@RequestBody ClientDTO clientDTO) {
		Client client = new Client.Builder().setName(clientDTO.getName()).setTypeClient(clientDTO.getTypeClient())
				.setCreditLimit(clientDTO.getCreditLimit()).build();

		clientService.save(client);

		clientDTO = new ClientDTO.Builder().setId(client.getId()).setTypeClient(client.getTypeClient().toString())
				.setCreditLimit(client.getCreditLimit()).setName(client.getName()).build();
		return new ResponseEntity<>(clientDTO, OK);

	}

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<?> findAll() {

		List<Client> clients = clientService.findAll();
		List<ClientDTO> listDTO = new ArrayList<ClientDTO>();

		for (Client client : clients) {
			listDTO.add(new ClientDTO.Builder().setId(client.getId()).setTypeClient(client.getTypeClient().toString())
					.setCreditLimit(client.getCreditLimit()).setName(client.getName()).build());
		}

		return new ResponseEntity<>(listDTO, OK);

	}
}
