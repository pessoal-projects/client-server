package br.com.luiz.carlos.test.klb.dto;

import java.math.BigDecimal;

public class ClientDTO {
	
	  private Long id;
      private String name;
      private String typeClient;
      private BigDecimal creditLimit;
	
	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(BigDecimal creditLimit) {
		this.creditLimit = creditLimit;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
	
	

	public String getTypeClient() {
		return typeClient;
	}

	public void setTypeClient(String typeClient) {
		this.typeClient = typeClient;
	}

	


	public static class Builder {

        private Long id;
        private String name;
        private String typeClient;
        private BigDecimal creditLimit;
    	

        public Builder setId(Long id) {
            this.id = id;
            return this; 
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }
        
        public Builder setTypeClient(String type) {
            this.typeClient = type;
            return this;
        }
        
        public Builder setCreditLimit(BigDecimal creditLimit) {
            this.creditLimit = creditLimit;
            return this;
        }

        public ClientDTO build(){
        	ClientDTO clientDTO= new ClientDTO();
        	clientDTO.setName(this.name);
        	clientDTO.setId(this.id);
        	clientDTO.setTypeClient(this.typeClient);
        	clientDTO.setCreditLimit(this.creditLimit);
        	return clientDTO;
        }
    }

	 

}
