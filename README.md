# Client Server



## POST
URL: http://localhost:8080/add

`{
	"name":"Maria",
	"typeClient":"A",
	"creditLimit":998.0
}`


## Get

URL:http://localhost:8080/ 

`
 [
	{
		"name":"Maria",
		"typeClient":"A",
		"creditLimit":887.09
	},
  {
  		"name":"Jose",
  		"typeClient":"B",
  		"creditLimit":878.98
  	}
]`